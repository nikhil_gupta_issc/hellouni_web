import React, { Component } from "react";
import Footer from "./footer.jsx";
import { Link } from "react-router-dom";
class Degree extends Component {
  constructor(props) {
    super();
    this.state = {
      selected_countries: localStorage.getItem("selected_countries"),
      subcourse: localStorage.getItem("subcourse"),
      imgValue:localStorage.getItem("imgValue")
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.selectedValue = this.selectedValue.bind(this);
  }
  componentDidMount() {
    fetch("http://sampleapi:8080/get/degree", {
      method: "GET",
      dataType: "JSON",
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(resp => {
        return resp.json();
      })
      .then(data => {
        this.setState({ degree: data });
        console.log(this.state.degree);
      })
      .catch(error => {
        console.log(error);
      });
  }
  handleSubmit() {
    console.log(this.state.imgValue,"---------------------");
    window.location.assign("/user/lead");
  }

  selectedValue(event) {
    let a= event.target.id;
    console.log(a);
    localStorage.setItem('imgValue', a);
  }
  render() {
    let degreeImagelist;
    if (this.state.degree) {
      degreeImagelist = this.state.degree.map(val => {
        const degreeName = val.name.split("/")[0];
        const degreeImgName = "/img/" + val.name.split("/")[0] + ".png";
        return (
          <div
            className="col-sm-3 grayscale stream"
            id={val.id}
            name={degreeName}
          >
            <div className="animation">
              <label style={{ cursor: "pointer" }}>
                <img src={degreeImgName} onClick={this.selectedValue} id={val.id}/>
              </label>
            </div>
          </div>
        );
      });
    }
    return (
      <div className="app">
        <nav
          className="navbar navbar-custom  top-nav-collapse"
          role="navigation"
        >
          <div
            className="row"
            style={{
              backgroundColor: "#3C3C3C",
              padding: "2px 0px",
              borderBottom: "1px solid #A4A4A4"
            }}
          >
            <div className="container" style={{ backgroundColor: "#3C3C3C" }}>
              <div className="pull-left header_icons">
                <ul style={{ paddingLeft: "0px" }}>
                  <li>
                    <img src="/img/1.png" />
                    NIKHIL.GUPTA@ISSC.IN
                  </li>
                  <li>
                    <img src="/img/2.png" /> +91 9167991339 / +91 9819900666
                  </li>
                </ul>
              </div>
              <div className="pull-right hidden-xs header_icons">
                <ul>
                  <li>
                    <img src="/img/f.png" />
                  </li>
                  <li>
                    <img src="/img/t.png" />
                  </li>
                  <li>
                    <img src="/img/g.png" />
                  </li>
                  <li>
                    <img src="/img/in.png" />
                  </li>
                  <li>
                    <Link to="/user/account/">
                      <img src="/img/login.png" />
                      <span
                        style={{ fontSize: "11px", padding: "0", margin: "0" }}
                      >
                        {" "}
                        LOGIN
                      </span>
                    </Link>
                  </li>
                  <li>
                    {" "}
                    <img src="/img/fav.png" />
                    <span
                      style={{ fontSize: "11px", padding: "0", margin: "0" }}
                    >
                      FAVOURITES
                    </span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="container">
            <div className="navbar-header">
              <button
                type="button"
                className="navbar-toggle"
                data-toggle="collapse"
                data-target=".navbar-main-collapse"
              >
                <i className="fa fa-bars" />
              </button>
              <a className="navbar-brand page-scroll" href="#page-top">
                <img src="/img/logo.png" width="200" />
              </a>
            </div>
            <div
              className="collapse navbar-collapse navbar-right navbar-main-collapse"
              style={{ marginTop: "10px", paddingRight: "0px" }}
            >
              <ul className="nav navbar-nav">
                <li className="hidden">
                  <a href="#page-top" />
                </li>
                <li>
                  <a className="page-scroll" href="/">
                    HOME
                  </a>
                </li>
                <li>
                  <Link to="/university">UNIVERSITY</Link>
                </li>
                {/* <li>
                    <Link to="/about">About</Link>
                    </li> */}
                <li>
                  <Link to="/researches">Research insights</Link>
                </li>
                <li>
                  <Link to="/friends-n-alumns">Friends & alumnus</Link>
                </li>
                <li>
                  <Link to="/join-a-group">join-a-group</Link>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        <div className="clearfix" />

        <div id="page_loader">
          {" "}
          <div className="container hidden-xs" style={{ padding: "17px" }}>
            <img src="/img/page1.jpg" width="100%" />
          </div>
          <div
            className="container visible-xs "
            style={{ textAlign: "center" }}
          >
            <img src="/img/page1-1.jpg" />
          </div>
          {/* <div className="shade_border"></div> */}
        </div>
        <div className="container hidden-xs hidden-sm hidden-md">
          <h6 style={{ color: "#3C3C3C" }}>
            Selected Countries - {this.state.selected_countries}
          </h6>
          <h6 style={{ color: "#3C3C3C" }}>
            Selected Streams - {this.state.subcourse}
          </h6>

          <h4 style={{ color: "#000" }}>Level of Course</h4>

          <input type="hidden" id="course_no" valu="" />
          <div className="container">{degreeImagelist}</div>

          <div className="htmlForm-group">
            <button
              name="submit"
              className="btn"
              id="next_resp"
              onClick={this.handleSubmit}
              style={{
                backgroundColor: "#F6881F",
                borderRadius: "2px",
                float: "right"
              }}
            >
              Next
            </button>
          </div>
        </div>

        <br />
        <Footer />
      </div>
    );
  }
}

export default Degree;
