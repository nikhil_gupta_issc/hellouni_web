import React, { Component } from "react";
import Footer from "./footer.jsx";
import { Link } from "react-router-dom";
import Modal from "react-modal";
const customStyles = {
  content: {
    top: "50%",
    left: "50%",
    right: "auto",
    bottom: "auto",
    marginRight: "-50%",
    transform: "translate(-50%, -50%)",
    background: "#eee"
  }
};
class Course extends Component {
  constructor(props) {
    super();
    this.state = {
      a: [],
      selected_countries: localStorage.getItem("selected_countries"),
      modalIsOpen: false
    };
    this.subcourseSelected = [];
    this.handleSubmit = this.handleSubmit.bind(this);
    this.openModal = this.openModal.bind(this);
    this.afterOpenModal = this.afterOpenModal.bind(this);
    this.closeModal = this.closeModal.bind(this);
    this.handleCourseChange = this.handleCourseChange.bind(this);
    //this.handleSubcourse = this.handleSubcourse.bind(this);
  }
  openModal(id, name, imgName) {
    console.log(id);
    let mainstreamValue = id;
    console.log(mainstreamValue);
    localStorage.setItem('mainstream',mainstreamValue);
    fetch("http://sampleapi:8080/subcourse/" + id, {
      method: "GET",
      dataType: "JSON",
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(resp => {
        return resp.json();
      })
      .then(data => {
        console.log(data);
        this.setState({ subcourse: data });
      })
      .catch(error => {
        console.log(error);
      });
    this.setState({ courseName: name });
    this.setState({ courseImgName: imgName });
    this.setState({ modalIsOpen: true });
  }

  afterOpenModal() {
    // references are now sync'd and can be accessed.
    this.subtitle.style.color = "black";
  }

  closeModal() {
    this.setState({ modalIsOpen: false });
  }

  handleSubmit() {
    console.log(
      this.subcourseSelected,
      "---------------------"
    );
    // let unique = [...new Set(this.subcourseSelected)];
    // console.log(unique)
    localStorage.setItem("subcourse", this.subcourseSelected);
    window.location.assign("/course/degree");
  }

  handleCourseChange() {
    console.log(this.subcourse.value,'--------------');
    this.subcourseSelected.push(this.subcourse.value);
  }

  componentDidMount() {
    Modal.setAppElement("body");
    fetch("http://sampleapi:8080/course/all", {
      method: "GET",
      dataType: "JSON",
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(resp => {
        return resp.json();
      })
      .then(data => {
        this.setState({ course: data });

        let courseDetailsList = data.map(function(val) {
          return val.id;
        });
        let imagesData = [];
        
        this.setState({ courseDetailsList: courseDetailsList });
        this.setState({ courseFullList: data });
        this.setState({ imagesData: imagesData });
        console.log(this.state.course);
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    let coursesImagelist;
    let subCourseslist;
    if (this.state.subcourse) {
      subCourseslist = this.state.subcourse.map(val => {
        return <option value={val.idd}>{val.name}</option>;
      });
    }
    if (this.state.course) {
      coursesImagelist = this.state.course.map(val => {
        const id = val.id;
        const courseName = val.name.split("/")[0];
        const courseImgName = "/img/" + val.name.split("/")[0] + ".png";
        return (
          <div
            className="col-lg-4 grayscale stream"
            id={val.id}
            name={courseName}
          >
            <div className="animation">
              <a
                className=""
                onClick={() => this.openModal(id, courseName, courseImgName)}
              >
                <label style={{ cursor: "pointer" }}>
                  <img src={courseImgName} /> 
                </label>
              </a>
            </div>
          </div>
        );
      });
    }
    return (
      <div className="app">
        <nav
          className="navbar navbar-custom  top-nav-collapse"
          role="navigation"
        >
          <div
            className="row"
            style={{
              backgroundColor: "#3C3C3C",
              padding: "2px 0px",
              borderBottom: "1px solid #A4A4A4"
            }}
          >
            <div className="container" style={{ backgroundColor: "#3C3C3C" }}>
              <div className="pull-left header_icons">
                <ul style={{ paddingLeft: "0px" }}>
                  <li>
                    <img src="/img/1.png" />
                    NIKHIL.GUPTA@ISSC.IN
                  </li>
                  <li>
                    <img src="/img/2.png" /> +91 9167991339 / +91 9819900666
                  </li>
                </ul>
              </div>
              <div className="pull-right hidden-xs header_icons">
                <ul>
                  <li>
                    <img src="/img/f.png" />
                  </li>
                  <li>
                    <img src="/img/t.png" />
                  </li>
                  <li>
                    <img src="/img/g.png" />
                  </li>
                  <li>
                    <img src="/img/in.png" />
                  </li>
                  <li>
                    <Link to="/user/account/">
                      <img src="/img/login.png" />
                      <span
                        style={{ fontSize: "11px", padding: "0", margin: "0" }}
                      >
                        {" "}
                        LOGIN
                      </span>
                    </Link>
                  </li>
                  <li>
                    {" "}
                    <img src="/img/fav.png" />
                    <span
                      style={{ fontSize: "11px", padding: "0", margin: "0" }}
                    >
                      FAVOURITES
                    </span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="container">
            <div className="navbar-header">
              <button
                type="button"
                className="navbar-toggle"
                data-toggle="collapse"
                data-target=".navbar-main-collapse"
              >
                <i className="fa fa-bars" />
              </button>
              <a className="navbar-brand page-scroll" href="#page-top">
                <img src="/img/logo.png" width="200" />
              </a>
            </div>
            <div
              className="collapse navbar-collapse navbar-right navbar-main-collapse"
              style={{ marginTop: "10px", paddingRight: "0px" }}
            >
              <ul className="nav navbar-nav">
                <li className="hidden">
                  <a href="#page-top" />
                </li>
                <li>
                  <a className="page-scroll" href="/">
                    HOME
                  </a>
                </li>
                <li>
                  <Link to="/university">UNIVERSITY</Link>
                </li>
                {/* <li>
                    <Link to="/about">About</Link>
                    </li> */}
                <li>
                  <Link to="/researches">Research insights</Link>
                </li>
                <li>
                  <Link to="/friends-n-alumns">Friends & alumnus</Link>
                </li>
                <li>
                  <Link to="/join-a-group">join-a-group</Link>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        <div className="clearfix" />

        <div id="page_loader">
          {" "}
          <div className="container hidden-xs" style={{ padding: "17px" }}>
            <img src="/img/page1.jpg" width="100%" />
          </div>
          <div
            className="container visible-xs "
            style={{ textAlign: "center" }}
          >
            <img src="/img/page1-1.jpg" />
          </div>
          {/* <div className="shade_border"></div> */}
        </div>

        <div className="container hidden-xs hidden-sm hidden-md">
          <h6 style={{ color: "#3C3C3C" }}>
            Selected Countries - {this.state.selected_countries}
          </h6>
          <h4 style={{ color: "#3C3C3C" }}>
            SELECT MAINstream - {this.state.modalIsOpen}
          </h4>
          <input type="hidden" id="course_no" valu="" />
          <div className="container">{coursesImagelist}</div>
          <div className="form-group">
            <button
              name="submit"
              className="btn"
              id="next_resp"
              style={{
                backgroundColor: "#F6881F",
                borderRadius: "2px",
                float: "right"
              }}
            >
              Next
            </button>
          </div>
        </div>
        <Modal
          isOpen={this.state.modalIsOpen}
          onAfterOpen={this.afterOpenModal}
          onRequestClose={this.closeModal}
          style={customStyles}
          contentLabel="Courses"
        >
          <div style={{ float: "left" }}>
            <img src={this.state.courseImgName} />
          </div>

          <div style={{ float: "left", textColor: "black" }}>
            <h6 ref={subtitle => (this.subtitle = subtitle)}>
              {this.state.courseName}
            </h6>
            <select
              multiple
              style={{ height: "125px" }}
              ref={input => (this.subcourse = input)}
              onChange = {this.handleCourseChange}
            >
              {subCourseslist}
            </select>
            {/* <input
              type="hidden"
              name="subcourse"
              id="subcourse"
              ref={input => (this.subcourse = input)}
            /> */}
            <div style={{ textAlign: "center" }}>
              <button
                name="button"
                className="btn"
                id="next_resp"
                onClick={this.handleSubmit}
                style={{
                  backgroundColor: "#F6881F",
                  borderRadius: "2px",
                  marginLeft: "90px",
                  marginTop: "5px"
                }}
              >
                Next
              </button>
            </div>
          </div>
        </Modal>
        <br />
        <Footer />
      </div>
    );
  }
}

Course.defaultProps = {
  headerProp: "Header from props...",
  contentProp: "Content from props..."
};

export default Course;
