import React, { Component } from "react";
import Header from './header.jsx';
import Footer from './footer.jsx';

class Guidance extends Component {
  render() {
    return (
      <div className="app">
        <Header />
        <div className="shade_border" />
        <br />
        <Footer />
      </div>
    );
  }
}

export default Guidance;