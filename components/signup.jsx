import React, { Component } from "react";
import Footer from "./footer.jsx";
import { Link } from "react-router-dom";

class Signup extends Component {
  constructor(props) {
    super(props);
    this.state = {
      countries: [],
      countryId: 0,
      stateZoneId: 0,
      a: [],
      intake: [],
      x: []
    };
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleCountryEvent = this.handleCountryEvent.bind(this);
  }

  handleSubmit() {
    console.log(
      this.selected_countries.value,
      "---------------------",
      this.selected_countries
    );
    localStorage.setItem("selected_countries", this.selected_countries.value);
    window.location.assign("/course/course");
  }

  componentDidMount() {
    fetch("http://sampleapi:8080/country/all", {
      method: "GET",
      dataType: "JSON",
      headers: {
        "Content-Type": "application/json"
      }
    })
      .then(resp => {
        return resp.json();
      })
      .then(data => {
        this.setState({ countries: data });
        // console.log(data);
        // console.log(this.state.countries);
        let countriesList = data
          .map(function(val) {
            return val.code;
          })
          .join(",");
        this.setState({ countriesList: countriesList });
      })
      .catch(error => {
        console.log(error);
      });
  }
  handleCountryEvent(event) {
    let countries = event.target.value;
    console.log(countries);
    this.setState({
      a: event.target.value
    });
    console.log(a);
  }
  render() {
    return (
      <div className="app">
        <nav
          className="navbar navbar-custom  top-nav-collapse"
          role="navigation"
        >
          <div
            className="row"
            style={{
              backgroundColor: "#3C3C3C",
              padding: "2px 0px",
              borderBottom: "1px solid #A4A4A4"
            }}
          >
            <div className="container" style={{ backgroundColor: "#3C3C3C" }}>
              <div className="pull-left header_icons">
                <ul style={{ paddingLeft: "0px" }}>
                  <li>
                    <img src="/img/1.png" />
                    NIKHIL.GUPTA@ISSC.IN
                  </li>
                  <li>
                    <img src="/img/2.png" /> +91 9167991339 / +91 9819900666
                  </li>
                </ul>
              </div>
              <div className="pull-right hidden-xs header_icons">
                <ul>
                  <li>
                    <img src="/img/f.png" />
                  </li>
                  <li>
                    <img src="/img/t.png" />
                  </li>
                  <li>
                    <img src="/img/g.png" />
                  </li>
                  <li>
                    <img src="/img/in.png" />
                  </li>
                  <li>
                    <Link to="/user/account/">
                      <img src="/img/login.png" />
                      <span
                        style={{ fontSize: "11px", padding: "0", margin: "0" }}
                      >
                        {" "}
                        LOGIN
                      </span>
                    </Link>
                  </li>
                  <li>
                    {" "}
                    <img src="/img/fav.png" />
                    <span
                      style={{ fontSize: "11px", padding: "0", margin: "0" }}
                    >
                      FAVOURITES
                    </span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="container">
            <div className="navbar-header">
              <button
                type="button"
                className="navbar-toggle"
                data-toggle="collapse"
                data-target=".navbar-main-collapse"
              >
                <i className="fa fa-bars" />
              </button>
              <a className="navbar-brand page-scroll" href="#page-top">
                <img src="/img/logo.png" width="200" />
              </a>
            </div>
            <div
              className="collapse navbar-collapse navbar-right navbar-main-collapse"
              style={{ marginTop: "10px", paddingRight: "0px" }}
            >
              <ul className="nav navbar-nav">
                <li className="hidden">
                  <a href="#page-top" />
                </li>
                <li>
                  <a className="page-scroll" href="/">
                    HOME
                  </a>
                </li>
                <li>
                  <Link to="/university">UNIVERSITY</Link>
                </li>
                {/* <li>
                    <Link to="/about">About</Link>
                    </li> */}
                <li>
                  <Link to="/researches">Research insights</Link>
                </li>
                <li>
                  <Link to="/friends-n-alumns">Friends & alumnus</Link>
                </li>
                <li>
                  <Link to="/join-a-group">join-a-group</Link>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        <div className="clearfix" />

        <div id="page_loader">
          {" "}
          <div className="container hidden-xs" style={{ padding: "17px" }}>
            <img src="/img/page1.jpg" width="100%" />
          </div>
          <div
            className="container visible-xs "
            style={{ textAlign: "center" }}
          >
            <img src="/img/page1-1.jpg" />
          </div>
          {/* <div className="shade_border"></div>  */}
          <div className="container login_background" />
        </div>

        <div className="container hidden-xs hidden-sm">
          <h4 style={{ color: "#3C3C3C" }}>Choose Country</h4>
          <div id="vmap" style={{ height: "550px", marginLeft: "50px" }} />
          <input
            type="hidden"
            name="selected_countries"
            id="selected_countries"
            // onChange={this.handleCountryEvent}
            ref={input => (this.selected_countries = input)}
          />

          <input
            type="hidden"
            name="_countries"
            id="_countries"
            value={this.state.countriesList}
          />

          <button
            className="btn"
            type="submit"
            id="next"
            onClick={this.handleSubmit}
            style={{
              backgroundColor: "#F6881F",
              borderRadius: "2px",
              float: "right"
            }}
          >
            Next
          </button>
        </div>

        <br />
        <Footer />
      </div>
    );
  }
}
export default Signup;
