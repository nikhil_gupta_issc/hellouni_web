import React, { Component } from 'react';
import Footer from './footer.jsx';
import { Link } from 'react-router-dom';
class Home extends Component {
    render() {
        return (
            <div className="app">
            <nav className="navbar navbar-custom navbar-fixed-top" role="navigation">
        <div  className="row" >
        	<div className="container">
            	<div className="pull-left header_icons"><ul style={{paddingLeft:'0px'}}><li><img src="img/1.png"/>INFO@IMPERIAL-OVERSEAS.COM</li>
                 <li><img src="img/2.png"/> +91 9167991339 / +91 9819900666</li></ul></div>
                <div className="pull-right hidden-xs header_icons"><ul>
                <li><img src="img/f.png"/></li>
                <li><img src="img/t.png"/></li>
                <li><img src="img/g.png"/></li>
                <li><img src="img/in.png"/></li>
				 <li>  
                 <Link to="/user/account/"><img src="img/login.png"/><span style={{fontSize:'11px', padding:'0', margin:'0'}}> LOGIN</span></Link>
				</li>
                <li> <img src="img/fav.png"/><span style={{fontSize:'11px', padding:'0', margin:'0'}}>FAVOURITES</span></li>
                </ul></div>
            </div>
         </div>
         <div className="container">
            <div className="navbar-header">
                <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i className="fa fa-bars"></i>
                </button>
                <a className="navbar-brand page-scroll" href="#page-top">
                   <img src="img/logo.png" width="200" />
                </a>
            </div>
            <div className="collapse navbar-collapse navbar-right navbar-main-collapse" style={{marginTop: '10px'}}>
                <ul className="nav navbar-nav">
                    
                    <li className="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a className="page-scroll" href="/">HOME</a>
                    </li>
                    <li>
                     <Link to="/university">UNIVERSITY</Link>
                    </li>
                    {/* <li>
                    <Link to="/about">About</Link>
                    </li> */}
                    <li>
                    <Link to="/researches">Research insights</Link>
                    </li>
                     <li>
                    <Link to="/friends-n-alumns">Friends & alumnus</Link>
                    </li>
                     <li>
                     <Link to="/join-a-group">join-a-group</Link>
                    </li>
                </ul>
            </div>

        </div>
    </nav>
    <header className="intro">
        <div className="intro-body">
					<object width="100%" height="720" >
<param name="movie" value="https://www.youtube.com/embed/8wMs6gDg1-0"></param>
<param name="allowFullScreen" value="true"></param>
<param name="showcontrols" value="true"></param>
<param name="allowscriptaccess" value="always"></param>
<embed src="https://www.youtube.com/v/MIZV1jbvV24?modestbranding=1&amp;version=3&amp;hl=en_US&amp;autoplay=1&rel=0" type="application/x-shockwave-flash" width="100%" height="720" allowscriptaccess="always" allowFullScreen={true} showcontrols="true"></embed>
</object>
        </div>
    </header>
     <div className="container"><div className="col-lg-12"><img src="img/home1.jpg" width="100%"/></div></div>
     <Footer />
    </div>
        );
    }
}

export default Home;