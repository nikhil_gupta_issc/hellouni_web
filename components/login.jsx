import React, { Component } from "react";
import Footer from "./footer.jsx";
import Universitylist from "./universitylist.jsx";
import { Link } from "react-router-dom";
class login extends Component {
  constructor() {
    super();
    this.state = {
      username: "",
      password: ""
    };
    this.handleUsername = this.handleUsername.bind(this);
    this.handlePassword = this.handlePassword.bind(this);
  }
  handleUsername(event) {
    this.setState({ username: event.target.value });
  }
  handlePassword(event) {
    this.setState({ password: event.target.value });
  }
  login() {
    let obj = {};
    obj.username = this.state.username;
    obj.password = this.state.password;
    fetch("http://sampleapi:8080/login", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(obj)
    })
      .then(response => response.json())
      .then(responseJson => {
        if (responseJson.data) {
          // console.log(responseJson.data.login_object);
          this.props.history.push("/search/university");
        }
      })
      .catch(error => {
        console.error(error);
      });
  }

  render() {
    return (
      <div className="app">
        <nav
          className="navbar navbar-custom  top-nav-collapse"
          role="navigation"
        >
          <div
            className="row"
            style={{
              backgroundColor: "#3C3C3C",
              padding: "2px 0px",
              borderBottom: "1px solid #A4A4A4"
            }}
          >
            <div className="container" style={{ backgroundColor: "#3C3C3C" }}>
              <div className="pull-left header_icons">
                <ul style={{ paddingLeft: "0px" }}>
                  <li>
                    <img src="/img/1.png" />
                    NIKHIL.GUPTA@ISSC.IN
                  </li>
                  <li>
                    <img src="/img/2.png" /> +91 9167991339 / +91 9819900666
                  </li>
                </ul>
              </div>
              <div className="pull-right hidden-xs header_icons">
                <ul>
                  <li>
                    <img src="/img/f.png" />
                  </li>
                  <li>
                    <img src="/img/t.png" />
                  </li>
                  <li>
                    <img src="/img/g.png" />
                  </li>
                  <li>
                    <img src="/img/in.png" />
                  </li>
                  <li>
                    <Link to="/user/account/">
                      <img src="/img/login.png" />
                      <span
                        style={{ fontSize: "11px", padding: "0", margin: "0" }}
                      >
                        {" "}
                        LOGIN
                      </span>
                    </Link>
                  </li>
                  <li>
                    {" "}
                    <img src="/img/fav.png" />
                    <span
                      style={{ fontSize: "11px", padding: "0", margin: "0" }}
                    >
                      FAVOURITES
                    </span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="container">
            <div className="navbar-header">
              <button
                type="button"
                className="navbar-toggle"
                data-toggle="collapse"
                data-target=".navbar-main-collapse"
              >
                <i className="fa fa-bars" />
              </button>
              <a className="navbar-brand page-scroll" href="#page-top">
                <img src="/img/logo.png" width="200" />
              </a>
            </div>
            <div
              className="collapse navbar-collapse navbar-right navbar-main-collapse"
              style={{ marginTop: "10px", paddingRight: "0px" }}
            >
              <ul className="nav navbar-nav">
                <li className="hidden">
                  <a href="#page-top" />
                </li>
                <li>
                  <a className="page-scroll" href="/">
                    HOME
                  </a>
                </li>
                <li>
                  <Link to="/university">UNIVERSITY</Link>
                </li>
                {/* <li>
                    <Link to="/about">About</Link>
                    </li> */}
                <li>
                  <Link to="/researches">Research insights</Link>
                </li>
                <li>
                  <Link to="/friends-n-alumns">Friends & alumnus</Link>
                </li>
                <li>
                  <Link to="/join-a-group">join-a-group</Link>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        <div className="clearfix" />

        <div id="page_loader">
          {" "}
          <div className="container hidden-xs" style={{ padding: "17px" }}>
            <img src="/img/page1.jpg" width="100%" />
          </div>
          <div
            className="container visible-xs "
            style={{ textAlign: "center" }}
          >
            <img src="/img/page1-1.jpg" />
          </div>
          <div className="shade_border" />
          <div className="container login_background">
            <div className="col-lg-12 col-md-12">
              {" "}
              <h4 className="form_font_color">Welcome, Please Sign In </h4>
            </div>
            <div className="col-lg-4 col-md-4">
              <div className="form-group">
                <h4 className="login_head">LOGIN</h4>
              </div>
              <div className="form-group">
                {" "}
                <h5 className="login_head2">
                  <strong>Returning student</strong>
                </h5>
              </div>
              <span className="form_font_color">I already have an account</span>
              <form
                action="page5.php"
                className="form-group"
                id="input_container"
                method="post"
              >
                <div className="form-group ">
                  <input
                    type="text"
                    name="username"
                    placeholder="Username"
                    id="input"
                    className="form-control form_font_color"
                    style={{ height: "34px", width: "240px" }}
                    value={this.state.username}
                    onChange={this.handleUsername}
                  />
                  <img
                    src="/img/email.png"
                    id="input_img"
                    className="hidden-xs"
                    style={{ bottom: "170px" }}
                  />
                </div>

                <div className="form-group ">
                  <input
                    type="password"
                    name="password"
                    placeholder="Password"
                    id="input3"
                    className="form-control form_font_color"
                    style={{ height: "34px", width: "240px" }}
                    value={this.state.password}
                    onChange={this.handlePassword}
                  />
                  <img
                    src="/img/pwd.png"
                    id="input_img2"
                    className="hidden-xs"
                    style={{ bottom: "120px" }}
                  />
                </div>
                <div className="form-group">
                  <p className="form_font_color">
                    <a href="#">Forgot Password ?</a>
                  </p>
                </div>
                <div className="clearfix" />
                <div className="form-group">
                  <button
                    type="button"
                    className="button_sign_in"
                    onClick={() => {
                      this.login();
                    }}
                  >
                    SIGN IN
                  </button>
                  &nbsp;
                </div>
              </form>
            </div>
            <div className="col-lg-4 col-md-4">
              <h4 className="login_head">New Student</h4>
              <div className="form-group form_font_color">
                {" "}
                <h5 className="form_font_color">
                  <strong>I would like to Sign up</strong>
                </h5>
              </div>
              <p className="form_font_color">
                By creating an account at HELLOUNI you will be able to shop
                faster, be up to date on an orders status, and keep track of the
                orders you have previously made.
              </p>
              <button type="button" className="button_sign_in">
                <Link to="/user/account/signup">SIGN UP</Link>
              </button>
            </div>
            <div className="col-lg-1 hidden-xs hidden-sm hidden-md">
              <div
                style={{
                  background: "url(/img/line2.png)",
                  repeat: "y",
                  height: "100px"
                }}
              />
              <img src="/img/or.png" width="37" />
              <div className="sign_line" />
            </div>
            <div className="col-lg-3 col-md-4">
              <div className="content-section" style={{ paddingTop: "50px" }}>
                <div>
                  <img src="/img/facebook-signup.png" />
                </div>
                <div>
                  <img src="/img/google-signup.png" />
                </div>
              </div>
            </div>
          </div>
        </div>

        <br />
        <Footer />
      </div>
    );
  }
}

export default login;
