import React, { Component } from 'react';
import Footer from './footer.jsx';
import Header from './header.jsx';
import { Link } from 'react-router-dom';

class Join extends Component {
    render() {
        return (
            <div className="app">
                <nav className="navbar navbar-custom  top-nav-collapse" role="navigation">
      <div style={{backgroundColor:'#3C3C3C' , padding: '2px 0px' , borderBottom: '1px solid #A4A4A4'}}>
        	<div className="container" style={{backgroundColor:'#3C3C3C'}}>
            	<div className="pull-left header_icons"><ul style={{paddingLeft:'0px'}}><li><img src="img/1.png"/>NIKHIL.GUPTA@ISSC.IN</li>
                 <li><img src="img/2.png"/> +91 9167991339 / +91 9819900666</li></ul></div>
                <div className="pull-right hidden-xs header_icons"><ul>
                <li><img src="img/f.png"/></li>
                <li><img src="img/t.png"/></li>
                <li><img src="img/g.png"/></li>
                <li><img src="img/in.png"/></li>
				 <li>  
				 <Link to="/user/account/"><img src="img/login.png"/><span style={{fontSize:'11px', padding:'0', margin:'0'}}> LOGIN</span></Link>
				</li>
                <li> <img src="img/fav.png"/><span style={{fontSize:'11px', padding:'0', margin:'0'}}>FAVOURITES</span></li>
                </ul></div>
            </div>
         </div>
         <div className="container">
         <div className="navbar-header">
                <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i className="fa fa-bars"></i>
                </button>
                <a className="navbar-brand page-scroll" href="#page-top">
                   <img src="img/logo.png" width="200"/>
                </a>
            </div>
            <div className="collapse navbar-collapse navbar-right navbar-main-collapse" style={{marginTop: '10px', paddingRight:'0px'}}>
                <ul className="nav navbar-nav">
                    
                    <li className="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a className="page-scroll" href="/">HOME</a>
                    </li>
                    <li>
                     <Link to="/university">UNIVERSITY</Link>
                    </li>
                    {/* <li>
                    <Link to="/about">About</Link>
                    </li> */}
                    <li>
                    <Link to="/researches">Research insights</Link>
                    </li>
                     <li>
                    <Link to="/friends-n-alumns">Friends & alumnus</Link>
                    </li>
                     <li>
                     <Link to="/join-a-group">join-a-group</Link>
                    </li>
                </ul>
            </div>

        </div>
    </nav>
      <div className="container" style={{margin:'30px auto'}}>
        <div className="row">
            

			<div className="col-md-6 text-left">
				<h3>Join a Group</h3>
			</div>
			{/* <!--div className="col-md-6 text-right frnd-search ">
				Search <input type="search" placeholder="friends / alumns" autocomplete="off" >
			</div--> */}
	</div>		
</div>		
			
<div className="container"  style={{margin:'30px auto'}}>
        
            <div className="row">
			<div className="col-md-8 text-left">
			<table className="table table-bordered">
  <thead>
    <tr>
      <th>Group Name</th>
      <th>WhatsApp Group Link</th>
    </tr>
  </thead>
  <tbody>
	<tr><td>Northeastern University</td> <td><a href="https://chat.whatsapp.com/JLJGdVIFJqMEKhfLSJZr1J" target="_blank">https://chat.whatsapp.com/JLJGdVIFJqMEKhfLSJZr1J </a></td> </tr>
	<tr><td>University of Texas Arlington	</td><td><a href="https://chat.whatsapp.com/EqYbkbhejRUCwhQkupGpTb " target="_blank">https://chat.whatsapp.com/EqYbkbhejRUCwhQkupGpTb </a></td></tr>
	<tr><td>University of South Florida </td><td> <a href="https://chat.whatsapp.com/KDswNg1B2itI4Mo9H6C3xY" target="_blank">https://chat.whatsapp.com/KDswNg1B2itI4Mo9H6C3xY </a></td></tr>
	<tr><td>Arizona State University </td><td> <a href="https://chat.whatsapp.com/Jg1jETXzLGLE9OdrvvLcBq" target="_blank">https://chat.whatsapp.com/Jg1jETXzLGLE9OdrvvLcBq</a></td></tr>
	<tr><td>New Jersey Institute of Technology </td><td> <a href="https://chat.whatsapp.com/K2bic7SkDNVIyWMu0xZrDR " target="_blank">https://chat.whatsapp.com/K2bic7SkDNVIyWMu0xZrDR </a></td></tr>
	<tr><td>University of Maryland Baltimore county </td><td> 	<a href="https://chat.whatsapp.com/KfKGPDpYrkmDeLPRZLxdZF " target="_blank">https://chat.whatsapp.com/KfKGPDpYrkmDeLPRZLxdZF </a></td></tr>
	<tr><td>San Jose State University </td><td><a href="https://chat.whatsapp.com/KyxH55oV3ksEa6ZKAn4YgK" target="_blank">https://chat.whatsapp.com/KyxH55oV3ksEa6ZKAn4YgK </a></td></tr>
	<tr><td>California State University Fullerton </td><td><a href="https://chat.whatsapp.com/IdHWajeyyM35VqjmklOsVn " target="_blank">https://chat.whatsapp.com/IdHWajeyyM35VqjmklOsVn </a></td></tr>
	<tr><td>Pace University </td><td><a href="https://chat.whatsapp.com/KxNBX8CB9H1IKY6hMbw7LS " target="_blank">https://chat.whatsapp.com/KxNBX8CB9H1IKY6hMbw7LS </a></td></tr>
	<tr><td>California State University Long beach </td><td><a href="https://chat.whatsapp.com/FGjcwHzoaTL0e9ux6SnlMb " target="_blank">https://chat.whatsapp.com/FGjcwHzoaTL0e9ux6SnlMb </a></td></tr>
	<tr><td>San Francisco State University </td><td><a href="https://chat.whatsapp.com/GrfQxZephTRCFlbgEPXgdw " target="_blank">https://chat.whatsapp.com/GrfQxZephTRCFlbgEPXgdw </a></td></tr>
	<tr><td>University of Massachusetts Lowell </td><td><a href="https://chat.whatsapp.com/COyQunuIEUOIXh8ayQcvgA" target="_blank">https://chat.whatsapp.com/COyQunuIEUOIXh8ayQcvgA </a></td></tr>
	<tr><td>University of Massachusetts Dartmouth </td><td><a href="https://chat.whatsapp.com/IqkjY8YoimCHuTCZWYcl1v" target="_blank">https://chat.whatsapp.com/IqkjY8YoimCHuTCZWYcl1v </a></td></tr>
	<tr><td>New York Institute of Technology </td><td><a href="https://chat.whatsapp.com/FqUlg0cvmXaD2k2Mb4T66r" target="_blank">https://chat.whatsapp.com/FqUlg0cvmXaD2k2Mb4T66r </a></td></tr>
	<tr><td>California State University East Bay </td><td><a href="https://chat.whatsapp.com/Cx1nxQ9Gh5FEcwV0E1WQln" target="_blank">https://chat.whatsapp.com/Cx1nxQ9Gh5FEcwV0E1WQln </a></td></tr>
	<tr><td>MCPHS University </td><td> <a href="https://chat.whatsapp.com/EftWJGPBHeG2cORBScGqVN " target="_blank">https://chat.whatsapp.com/EftWJGPBHeG2cORBScGqVN </a></td></tr>
	<tr><td>University at Albany (SUNY) </td><td><a href="https://chat.whatsapp.com/CKx6OsG61opCLvxzh2EzfP" target="_blank">https://chat.whatsapp.com/CKx6OsG61opCLvxzh2EzfP </a></td></tr>
	<tr><td>University of Bridgeport </td><td><a href="https://chat.whatsapp.com/KBfLrmZpHlVCyWFQPrcZcV " target="_blank">https://chat.whatsapp.com/KBfLrmZpHlVCyWFQPrcZcV </a></td></tr>
	<tr><td>Binghamton University (SUNY) </td><td><a href="https://chat.whatsapp.com/CDalAI67ctL2fdDj6J6xWN " target="_blank">https://chat.whatsapp.com/CDalAI67ctL2fdDj6J6xWN </a></td></tr>
	<tr><td>University of Colorado Denver </td><td><a href="https://chat.whatsapp.com/BBBHFdQejTQERqdIv5mQ5o" target="_blank">https://chat.whatsapp.com/BBBHFdQejTQERqdIv5mQ5o </a></td></tr>
	<tr><td>SUNY Polytechnic Institute </td><td><a href="https://chat.whatsapp.com/BhP3KHhco7x7pdT7fJEb41" target="_blank">https://chat.whatsapp.com/BhP3KHhco7x7pdT7fJEb41 </a></td></tr>
	<tr><td>University of North Texas </td><td><a href="https://chat.whatsapp.com/EMSvBg6uYzeIh5iCc3rNSv" target="_blank">https://chat.whatsapp.com/EMSvBg6uYzeIh5iCc3rNSv </a></td></tr>
</tbody>
</table>
</div>	
                   			
            </div>		
</div>		









   <br/>
   <Footer />
   </div>
        );
    }
}

export default Join;