import React, { Component } from "react";
import { Link } from 'react-router-dom';

class Footer extends Component {
  render() {
    return (
      <div>
        <footer style={{ backgroundColor: "#F6881F" }}>
          <div className="container">
            <div className="pull-left footer_menu">
              <ul style={{ marginLeft: "-40px" }}>
                <li><Link to="/about">about us</Link></li>
                <li><Link to="/additional-services">offers</Link></li>
                <li><Link to="/guidance">guidance</Link></li>
              </ul>
            </div>
            <div className="pull-right footer_menu">
              COPYRIGHT 2018 &copy; HELLOUNI 2018
            </div>
          </div>
        </footer>
      </div>
    );
  }
}

export default Footer;
