import React, { Component } from 'react';
import { Link } from 'react-router-dom';

class Header extends Component {
    render() {
        return (
            <div>
                <nav className="navbar navbar-custom  top-nav-collapse" role="navigation">
      <div style={{backgroundColor:'#3C3C3C' , padding: '2px 0px' , borderBottom: '1px solid #A4A4A4'}}>
        	<div className="container" style={{backgroundColor:'#3C3C3C'}}>
            	<div className="pull-left header_icons"><ul style={{paddingLeft:'0px'}}><li><img src="img/1.png"/>NIKHIL.GUPTA@ISSC.IN</li>
                 <li><img src="img/2.png"/> +91 9167991339 / +91 9819900666</li></ul></div>
                <div className="pull-right hidden-xs header_icons"><ul>
                <li><img src="img/f.png"/></li>
                <li><img src="img/t.png"/></li>
                <li><img src="img/g.png"/></li>
                <li><img src="img/in.png"/></li>
				 <li>  
				 <Link to="/user/account/"><img src="img/login.png"/><span style={{fontSize:'11px', padding:'0', margin:'0'}}> LOGIN</span></Link>
				</li>
                <li> <img src="img/fav.png"/><span style={{fontSize:'11px', padding:'0', margin:'0'}}>FAVOURITES</span></li>
                </ul></div>
            </div>
         </div>
         <div className="container">
         <div className="navbar-header">
                <button type="button" className="navbar-toggle" data-toggle="collapse" data-target=".navbar-main-collapse">
                    <i className="fa fa-bars"></i>
                </button>
                <a className="navbar-brand page-scroll" href="#page-top">
                   <img src="img/logo.png" width="200"/>
                </a>
            </div>
            <div className="collapse navbar-collapse navbar-right navbar-main-collapse" style={{marginTop: '10px', paddingRight:'0px'}}>
                <ul className="nav navbar-nav">
                    
                    <li className="hidden">
                        <a href="#page-top"></a>
                    </li>
                    <li>
                        <a className="page-scroll" href="/">HOME</a>
                    </li>
                    <li>
                     <Link to="/university">UNIVERSITY</Link>
                    </li>
                    {/* <li>
                    <Link to="/about">About</Link>
                    </li> */}
                    <li>
                    <Link to="/researches">Research insights</Link>
                    </li>
                     <li>
                    <Link to="/friends-n-alumns">Friends & alumnus</Link>
                    </li>
                     <li>
                     <Link to="/join-a-group">join-a-group</Link>
                    </li>
                </ul>
            </div>

        </div>
    </nav>
    <div className="clearfix"></div>
      <div id="page_loader"> <div   > 
            <img src="images/Terms_Conditions.jpg" width="100%"  />
          
       </div>
            </div>
            </div>
        );
    }
}

export default Header;