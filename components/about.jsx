import React, { Component } from 'react';
import Footer from './footer.jsx';
import Header from './header.jsx';
import { Link } from 'react-router-dom';

class About extends Component {
    render() {
        return (
            <div className="app">
      <Header />
  	 <div className="shade_border"></div>
        <div className="container">
        	<div className="col-lg-12 col-md-12 terms"> <h4 style={{color:'#000'}}>ABOUT US</h4>
            <p style={{color:'#000'}}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. </p>
             <h5 style={{color:'#000'}}>Lorem Ipsum</h5>
            <ul style={{color:'#000' , listStyle: 'none'}}>
            	<li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
            	<li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
                <li>Lorem Ipsum is simply dummy text of the printing and typesetting industry.</li>
            </ul>
          <p style={{color:'#000'}}>Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>  
            
            </div>
           	
     </div>	
   <br/>
   <Footer />
    </div>
        );
    }
}


export default About;