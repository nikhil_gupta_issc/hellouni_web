import React, { Component } from "react";
import Footer from "./footer.jsx";
import { Link } from "react-router-dom";

class Lead extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected_countries: localStorage.getItem("selected_countries"),
      subcourse: localStorage.getItem("subcourse"),
      imgValue:localStorage.getItem("imgValue"),
      mainstream:localStorage.getItem("mainstream")
    };
  }
  signup() {
    let signupDetails = {
      mainstream: this.state.mainstream,
      countries: this.state.selected_countries,
      courses: this.state.subcourse,
      degree: this.state.imgValue,
      name: this.name.value,
      email: this.email.value,
      phone: this.mobile.value
    };
    console.log(".................",signupDetails)
    fetch("http://sampleapi:8080/get/university", {
      method: "POST",
      headers: {
        "Content-Type": "application/json"
      },
      body: JSON.stringify(signupDetails)
    })
      .then(resp => {
        return resp.json();
      })
      .then(data => {
        this.setState({ signupDetails: data });
        if(this.state.signupDetails){
          // window.location.assign("/search/university");
        }
      })
      .catch(error => {
        console.log(error);
      });
  }

  render() {
    return (
      <div className="app">
        <nav
          className="navbar navbar-custom  top-nav-collapse"
          role="navigation"
        >
          <div
            className="row"
            style={{
              backgroundColor: "#3C3C3C",
              padding: "2px 0px",
              borderBottom: "1px solid #A4A4A4"
            }}
          >
            <div className="container" style={{ backgroundColor: "#3C3C3C" }}>
              <div className="pull-left header_icons">
                <ul style={{ paddingLeft: "0px" }}>
                  <li>
                    <img src="/img/1.png" />
                    NIKHIL.GUPTA@ISSC.IN
                  </li>
                  <li>
                    <img src="/img/2.png" /> +91 9167991339 / +91 9819900666
                  </li>
                </ul>
              </div>
              <div className="pull-right hidden-xs header_icons">
                <ul>
                  <li>
                    <img src="/img/f.png" />
                  </li>
                  <li>
                    <img src="/img/t.png" />
                  </li>
                  <li>
                    <img src="/img/g.png" />
                  </li>
                  <li>
                    <img src="/img/in.png" />
                  </li>
                  <li>
                    <Link to="/user/account/">
                      <img src="/img/login.png" />
                      <span
                        style={{ fontSize: "11px", padding: "0", margin: "0" }}
                      >
                        {" "}
                        LOGIN
                      </span>
                    </Link>
                  </li>
                  <li>
                    {" "}
                    <img src="/img/fav.png" />
                    <span
                      style={{ fontSize: "11px", padding: "0", margin: "0" }}
                    >
                      FAVOURITES
                    </span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="container">
            <div className="navbar-header">
              <button
                type="button"
                className="navbar-toggle"
                data-toggle="collapse"
                data-target=".navbar-main-collapse"
              >
                <i className="fa fa-bars" />
              </button>
              <a className="navbar-brand page-scroll" href="#page-top">
                <img src="/img/logo.png" width="200" />
              </a>
            </div>
            <div
              className="collapse navbar-collapse navbar-right navbar-main-collapse"
              style={{ marginTop: "10px", paddingRight: "0px" }}
            >
              <ul className="nav navbar-nav">
                <li className="hidden">
                  <a href="#page-top" />
                </li>
                <li>
                  <a className="page-scroll" href="/">
                    HOME
                  </a>
                </li>
                <li>
                  <Link to="/university">UNIVERSITY</Link>
                </li>
                {/* <li>
                    <Link to="/about">About</Link>
                    </li> */}
                <li>
                  <Link to="/researches">Research insights</Link>
                </li>
                <li>
                  <Link to="/friends-n-alumns">Friends & alumnus</Link>
                </li>
                <li>
                  <Link to="/join-a-group">join-a-group</Link>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        <div className="clearfix" />

        <div id="page_loader">
          {" "}
          <div className="container hidden-xs" style={{ padding: "17px" }}>
            <img src="/img/page1.jpg" width="100%" />
          </div>
          <div
            className="container visible-xs "
            style={{ textAlign: "center" }}
          >
            <img src="/img/page1-1.jpg" />
          </div>
          {/* <div className="shade_border"></div> */}
        </div>
        <div className="container container-table">
          <div className="container container-table">
            <div className="row vertical-center-row">
              <div className="col-md-4 col-md-offset-4 personal_info_heading">
                <h3>Personal Info</h3>
              </div>

              <div className="text-center col-md-4 col-md-offset-4 formbg">
                <form className="form-group" id="input_container" method="post">
                  <input type="hidden" name="countries" value="" />

                  <input type="hidden" name="mainstream" value="" />

                  <div className="form-group">
                    <span id="msg" />
                  </div>

                  <div className="form-group">
                    <input
                      type="text"
                      name="name"
                      placeholder="Name"
                      id="name"
                      className="form-control"
                      style={{ height: "34px" }}
                      ref={input => (this.name = input)}
                    />
                  </div>

                  <div className="form-group">
                    <input
                      type="text"
                      name="mobile"
                      placeholder="Mobile Number"
                      id="mobile"
                      className="form-control"
                      style={{ height: "34px" }}
                      ref={input => (this.mobile = input)}
                    />
                  </div>
                  <div className="form-group">
                    <input
                      type="text"
                      name="username"
                      placeholder="Username"
                      id="username"
                      className="form-control"
                      style={{ height: "34px" }}
                      ref={input => (this.username = input)}
                    />
                  </div>

                  <div className="form-group">
                    <input
                      type="text"
                      name="email"
                      placeholder="Email"
                      id="email"
                      className="form-control"
                      style={{ height: "34px" }}
                      ref={input => (this.email = input)}
                    />

                    <input
                      type="hidden"
                      name="existsmail"
                      id="existsmail"
                      value=""
                    />
                  </div>
                  <div className="form-group">
                    <input
                      type="password"
                      name="password"
                      placeholder="Password"
                      id="password"
                      className="form-control"
                      style={{ height: "34px" }}
                      ref={input => (this.password = input)}
                    />
                  </div>

                  <div className="clearfix" />

                  <div className="form-group">
                    <button type="button" className="back_btn">
                      BACK
                    </button>
                    &nbsp;
                    <input type="hidden" name="courses" value="" />
                    <input type="hidden" name="degree" value="" />
                    <button
                      type="button"
                      id="lead_submit"
                      className="go_btn"
                      onClick={() => {
                        this.signup();
                      }}
                    >
                      GO
                    </button>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <br />
        <Footer />
      </div>
    );
  }
}

export default Lead;
