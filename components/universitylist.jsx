import React, { Component } from "react";
import Footer from "./footer.jsx";
import { Link } from "react-router-dom";

class Universitylist extends Component {
  render() {
    return (
      <div className="app">
        <nav
          className="navbar navbar-custom  top-nav-collapse"
          role="navigation"
        >
          <div
            className="row"
            style={{
              backgroundColor: "#3C3C3C",
              padding: "2px 0px",
              borderBottom: "1px solid #A4A4A4"
            }}
          >
            <div className="container" style={{ backgroundColor: "#3C3C3C" }}>
              <div className="pull-left header_icons">
                <ul style={{ paddingLeft: "0px" }}>
                  <li>
                    <img src="/img/1.png" />
                    SUMANTA@INVENTIFWEB.COM
                  </li>
                  <li>
                    <img src="/img/2.png" /> +91 9167991339 / +91 9819900666
                  </li>
                </ul>
              </div>
              <div className="pull-right hidden-xs header_icons">
                <ul>
                  <li>
                    <img src="/img/f.png" />
                  </li>
                  <li>
                    <img src="/img/t.png" />
                  </li>
                  <li>
                    <img src="/img/g.png" />
                  </li>
                  <li>
                    <img src="/img/in.png" />
                  </li>
                  <li>
                    <Link to="/user/account/">
                      <img src="/img/login.png" />
                      <span
                        style={{ fontSize: "11px", padding: "0", margin: "0" }}
                      >
                        {" "}
                        LOGIN
                      </span>
                    </Link>
                  </li>
                  <li>
                    {" "}
                    <img src="/img/fav.png" />
                    <span
                      style={{ fontSize: "11px", padding: "0", margin: "0" }}
                    >
                      FAVOURITES
                    </span>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="container">
            <div className="navbar-header">
              <button
                type="button"
                className="navbar-toggle"
                data-toggle="collapse"
                data-target=".navbar-main-collapse"
              >
                <i className="fa fa-bars" />
              </button>
              <a className="navbar-brand page-scroll" href="#page-top">
                <img src="/img/logo.png" width="200" />
              </a>
            </div>
            <div
              className="collapse navbar-collapse navbar-right navbar-main-collapse"
              style={{ marginTop: "10px", paddingRight: "0px" }}
            >
              <ul className="nav navbar-nav">
                <li className="hidden">
                  <a href="#page-top" />
                </li>
                <li>
                  <a className="page-scroll" href="/">
                    HOME
                  </a>
                </li>
                <li>
                  <Link to="/university">UNIVERSITY</Link>
                </li>
                {/* <li>
                    <Link to="/about">About</Link>
                    </li> */}
                <li>
                  <Link to="/researches">Research insights</Link>
                </li>
                <li>
                  <Link to="/friends-n-alumns">Friends & alumnus</Link>
                </li>
                <li>
                  <Link to="/join-a-group">join-a-group</Link>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        <div className="clearfix" />

        <div id="page_loader">
          {" "}
          <div className="container hidden-xs" style={{ padding: "17px" }}>
            <img src="/img/page1.jpg" width="100%" />
          </div>
          <div
            className="container visible-xs "
            style={{ textAlign: "center" }}
          >
            <img src="/img/page1-1.jpg" />
          </div>
          <div className="shade_border" />
          <br />
          <div className="container">
            <div style={{ marginTop: "30px" }}>
              <div className="row search_form">
                <form
                  action="<?php echo base_url();?>search/university"
                  method="post"
                  name="searchform"
                  id="searchform"
                >
                  <div className="col-md-12">
                    <div className="col-lg-6">
                      <h4 className="pull-left">SEARCH BY</h4>
                    </div>
                  </div>

                  <div className="clearfix visible-xs visible-sm" />

                  <div className="col-md-12" style={{ color: "#000" }}>
                    <div className="col-md-4">
                      <label>COUNTRY *</label>&nbsp;
                      <select
                        name="country"
                        id="country"
                        className="searchoptions"
                        style={{
                          border: "none",
                          width: "70%",
                          height: "30px",
                          fontsize: "12px"
                        }}
                      >
                        <option value="0">Select Country</option>
                        <option value="India">INDIA</option>
                      </select>
                    </div>
                    <div className="clearfix visible-xs visible-sm">
                      <br />
                    </div>

                    <div className="col-md-4">
                      <label>MAINSTREAM *</label>&nbsp;
                      <select
                        name="mainstream"
                        id="mainstream"
                        className="searchoptions"
                        style={{
                          border: "none",
                          width: "70%",
                          height: "30px",
                          fontsize: "12px"
                        }}
                      >
                        <option value="0">Select Mainstream</option>
                      </select>
                    </div>
                    <div className="clearfix visible-xs visible-sm">
                      <br />
                    </div>

                    <div className="col-md-4">
                      <label>SUBSTREAM *</label>&nbsp;
                      <select
                        name="subcourse"
                        id="subcourse"
                        className="searchoptions"
                        style={{
                          border: "none",
                          width: "70%",
                          height: "30px",
                          fontsize: "12px"
                        }}
                      >
                        <option value="0">Select Substream</option>
                      </select>
                    </div>
                  </div>

                  <div className="col-md-12" style={{ color: "#000" }}>
                    {" "}
                    <br />
                    <div className="col-md-6">
                      <label>LEVEL OF COURSE *</label>&nbsp;
                      <select
                        name="degree"
                        id="degree"
                        className="searchoptions"
                        style={{
                          border: "none",
                          width: "70%",
                          height: "30px",
                          fontsize: "12px"
                        }}
                      >
                        <option value="0">Select Level Of Course</option>
                      </select>
                    </div>
                    <div className="col-lg-6 pull-left">
                      <input
                        type="submit"
                        value="SEARCH"
                        name="search"
                        id="search"
                        style={{
                          color: "#fff",
                          backgroundColor: "#f6881f",
                          border: "none",
                          padding: "8px 15px 8px 15px"
                        }}
                      />

                      <span id="errmsg" />
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>

        <br />
        <Footer />
      </div>
    );
  }
}

export default Universitylist;
