import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Footer from './components/footer.jsx';
import Signup from './components/signup.jsx';
import Login from './components/login.jsx';
import Join from './components/joinGroup.jsx';
import Research from './components/research.jsx';
import Friends from './components/friends.jsx';
import University from './components/university.jsx';
import About from './components/about.jsx';
import Home from './components/home.jsx';
import Universitylist from './components/universitylist.jsx';
import Header from './components/header.jsx';
import Course from './components/course.jsx';
import Degree from './components/degree.jsx';
import Lead from './components/lead.jsx';
import Guidance from './components/guidance.jsx';
import Offers from './components/offers.jsx';


class App extends Component {
    render() {
      return (
        <Router>
        <div>  
          <Route exact path="/" component={Home} />
          <Route exact path="/about" component={About} />
          <Route exact path="/researches" component={Research} />
          <Route exact path="/university" component={University} />
          <Route exact path="/friends-n-alumns" component={Friends} />
          <Route exact path="/join-a-group" component={Join} />
          <Route exact path="/user/account/" component={Login} />
          <Route exact path="/user/account/signup" component={Signup} />
          <Route exact path="/search/university" component={Universitylist} />
          <Route exact path="/course/course" component={Course} />
          <Route exact path="/course/degree" component={Degree} />
          <Route exact path="/user/lead" component={Lead} />
          <Route exact path="/guidance" component={Guidance} />
          <Route exact path="/additional-services" component={Offers} />
        </div>
      </Router>
      );
    }
  }

export default App;